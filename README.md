## Laravel 6 Image Sizer package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.2+
* PHP GD library
* [Composer](https://getcomposer.org/)

#### Use

Can be used standalone or with Laravel 6

* add service provider to `config/app.php`

```
'providers' => [
...
  Smorken\Image\Sizer\ServiceProvider::class,
```

Creating a resized image:

```
$stream = fopen('foo_image.png', 'r+');
//$string = file_get_contents('foo_image.png');
$sizer = app('Smorken\Image\Sizer\Contracts\Sizer');
$data = $sizer->size($stream);

$data = $sizer->size($stream, ['output' => 'png', 'base64' => true]);
return response()->make(
                $data['data'],
                200,
                [
                    'Content-Type'              => $data['mime'],
                    'Content-Transfer-Encoding' => 'binary',
                    'Content-Disposition'       => 'inline',
                    'Expires'                   => gmdate('D, d M Y H:i:s \G\M\T', time() + 86400),
                    'Cache-Control'             => 'max-age=86400, public',
                    'Pragma'                    => 'public',
                ]
            );
```

Data array:

```
 'mime' => 'image/jpg',
 'data' => binary_string, //(or base64 encoded string if base64 is true)
 'size' => 12345,
```

Default config settings:

```
 'height' => 200,
 'width'  => 200,
 'option' => 'auto',
 'output' => 'jpg',
 'base64' => false,
```
