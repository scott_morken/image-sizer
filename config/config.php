<?php

return [
    'height' => 200,
    'width' => 200,
    'option' => 'auto',
    'output' => 'jpg',
    'base64' => false,
];
