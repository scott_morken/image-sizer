<?php

namespace Smorken\Image\Sizer;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'imagesizer');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('imagesizer.php')], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \Smorken\Image\Sizer\Contracts\Sizer::class,
            function ($app) {
                $config = $app['config']->get('imagesizer', []);

                return new Sizer($config);
            }
        );
    }
}
