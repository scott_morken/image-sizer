<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/27/16
 * Time: 7:17 AM
 */

namespace Smorken\Image\Sizer\Contracts;

use Smorken\Image\Sizer\SizerException;

interface Sizer
{
    /**
     * Returns the config key or the entire config if the key is null
     */
    public function getConfig(?string $key = null): string|int|array|null;

    public function getMethod(string $output): string;

    public function getMime(string $output): string;

    /**
     * Turns an image stream/string into a string
     *
     * @param  resource|\GdImage|string  $input
     */
    public function parseImageInput(mixed $input): string;

    /**
     * Returns raw image resource
     *
     * @param  \GdImage  $image
     * @return resource|\GdImage|false
     */
    public function resizeImage(
        mixed $image,
        int $height,
        int $width,
        int $actual_height,
        int $actual_width,
        string $option = 'auto'
    );

    public function setConfig(array $config): void;

    public function setConfigOption(string $key, mixed $value): void;

    /**
     * Returns null or an array of
     * [
     *   'mime' => 'mime_type',
     *   'data' => 'image',
     *   'size' => 12345,
     * ]
     *
     * @param  resource|\GdImage|string  $input
     */
    public function size(mixed $input, array $options = []): ?Result;

    /**
     * @throws SizerException
     */
    public function validateSettings(): bool;
}
