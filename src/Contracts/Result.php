<?php

namespace Smorken\Image\Sizer\Contracts;

/**
 * @property string $mime
 * @property string $data
 * @property int $size
 *
 * @phpstan-require-extends \Smorken\Image\Sizer\Result
 */
interface Result extends \ArrayAccess
{
    public function getAttribute(string $key): mixed;

    public function setAttribute(string $key, mixed $value): void;
}
