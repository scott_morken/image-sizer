<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/27/16
 * Time: 7:17 AM
 */

namespace Smorken\Image\Sizer;

class Sizer implements \Smorken\Image\Sizer\Contracts\Sizer
{
    protected array $config = [
        'height' => 200,
        'width' => 200,
        'option' => 'auto',
        'output' => 'jpg',
        'base64' => false,
    ];

    protected array $options = [
        'auto' => 'Pick the optimal dimensions based on the image size',
        'crop' => 'Crop the image to fit the requested dimensions',
        'portrait' => 'Fixed height',
        'landscape' => 'Fixed width',
        'exact' => 'Exact height and width',
    ];

    protected array $outputs = [
        'jpg' => [
            'mime' => 'image/jpg',
            'method' => 'createJpg',
        ],
        'png' => [
            'mime' => 'image/png',
            'method' => 'createPng',
        ],
    ];

    public function __construct(array $config)
    {
        $this->setConfig($config);
    }

    public function getConfig(?string $key = null): string|int|array|null
    {
        if ($key) {
            return $this->config[$key] ?? null;
        }

        return $this->config;
    }

    public function setConfig(array $config): void
    {
        foreach ($config as $k => $v) {
            $this->setConfigOption($k, $v);
        }
    }

    public function getMethod(string $output): string
    {
        return $this->outputs[$output]['method'];
    }

    public function getMime(string $output): string
    {
        return $this->outputs[$output]['mime'];
    }

    public function parseImageInput(mixed $input): string
    {
        if (is_string($input)) {
            return $input;
        }
        if ($this->isStream($input)) {
            return stream_get_contents($input);
        }
        throw new SizerException('Invalid image input.  Image input must be a stream or string.');
    }

    public function resizeImage(
        mixed $image,
        int $height,
        int $width,
        int $actual_height,
        int $actual_width,
        string $option = 'auto'
    ) {
        // *** Get optimal width and height - based on $option
        [$optimal_width, $optimal_height] = $this->getDimensions($width, $height, $actual_width, $actual_height,
            strtolower($option));

        // *** Resample - create image canvas of x, y size
        $resized = imagecreatetruecolor($optimal_width, $optimal_height);
        $this->addAlphaLayers($image, $resized);
        imagecopyresampled($resized, $image, 0, 0, 0, 0, $optimal_width, $optimal_height, $actual_width,
            $actual_height);

        // *** if option is 'crop', then crop too
        if ($option == 'crop') {
            $this->crop($resized, $optimal_width, $optimal_height, $width, $height);
        }

        return $resized;
    }

    public function setConfigOption(string $key, mixed $value): void
    {
        if (! isset($this->config[$key])) {
            throw new SizerException("$key is not a valid configuration option.");
        }
        $this->config[$key] = strtolower((string) $value);
    }

    public function size(mixed $input, array $options = []): ?\Smorken\Image\Sizer\Contracts\Result
    {
        $this->setConfig($options);
        $this->validateSettings();
        $image_string = $this->parseImageInput($input);
        if ($image_string) {
            $resized = $this->resizeImageString($image_string);
            if ($resized) {
                $data = $this->getImageData($resized, $this->getConfig('output'));
                imagedestroy($resized);

                return new Result($data);
            }
        }

        return null;
    }

    public function validateSettings(): bool
    {
        return $this->validateOutputType($this->getConfig('output')) && $this->validateOption($this->getConfig('option'));
    }

    protected function addAlphaLayers($image, $resized): void
    {
        imagealphablending($resized, false);
        imagesavealpha($resized, true);
        imagealphablending($image, true);
    }

    protected function createImageFromString(string $image_string)
    {
        $image = @imagecreatefromstring($image_string);

        return $image;
    }

    protected function createJpg($image): string
    {
        ob_start();
        imagejpeg($image);

        return ob_get_clean();
    }

    protected function createPng($image): string
    {
        ob_start();
        imagepng($image);

        return ob_get_clean();
    }

    protected function crop($image, int $optimal_width, int $optimal_height, int $width, int $height)
    {
        // *** Find center - this will be used for the crop
        $crop_start_x = ($optimal_width / 2) - ($width / 2);
        $crop_start_y = ($optimal_height / 2) - ($height / 2);

        $crop = $image;
        // *** Now crop from center to exact requested size
        $image = imagecreatetruecolor($width, $height);
        $this->addAlphaLayers($image, $crop);
        imagecopyresampled($image, $crop, 0, 0, $crop_start_x, $crop_start_y, $width, $height, $width, $height);

        return $image;
    }

    protected function getDimensions(
        int $width,
        int $height,
        int $actual_width,
        int $actual_height,
        string $option
    ): array {
        switch ($option) {
            case 'portrait':
                [$optimal_width, $optimal_height] = $this->getSizeByFixedHeight($height, $actual_width,
                    $actual_height);
                break;
            case 'landscape':
                [$optimal_width, $optimal_height] = $this->getSizeByFixedWidth($width, $actual_width,
                    $actual_height);
                break;
            case 'auto':
                [$optimal_width, $optimal_height] = $this->getSizeByAuto($width, $height, $actual_width,
                    $actual_height);
                break;
            case 'crop':
                [$optimal_width, $optimal_height] = $this->getOptimalCrop($width, $height, $actual_width,
                    $actual_height);
                break;
            case 'exact':
            default:
                $optimal_width = $width;
                $optimal_height = $height;
                break;
        }

        return [$optimal_width, $optimal_height];
    }

    protected function getImageData($image, string $output): array
    {
        $method = $this->getMethod($output);
        $mime = $this->getMime($output);
        $data = null;
        if ($method && $mime) {
            $image_data = $this->$method($image);
            $data = [
                'mime' => $mime,
                'data' => $this->getConfig('base64') ? base64_encode((string) $image_data) : $image_data,
                'size' => strlen((string) $image_data),
            ];
        }

        return $data;
    }

    protected function getOptimalCrop(int $width, int $height, int $actual_width, int $actual_height): array
    {
        $heightRatio = $actual_height / $height;
        $widthRatio = $actual_width / $width;

        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        } else {
            $optimalRatio = $widthRatio;
        }

        $optimal_height = $actual_height / $optimalRatio;
        $optimal_width = $actual_width / $optimalRatio;

        return [$optimal_width, $optimal_height];
    }

    protected function getSizeAsLandscape(int $width, int $actual_width, int $actual_height): array
    {
        return $this->getSizeByFixedWidth($width, $actual_width, $actual_height);
    }

    protected function getSizeAsPortrait(int $height, int $actual_width, int $actual_height): array
    {
        return $this->getSizeByFixedHeight($height, $actual_width, $actual_height);
    }

    protected function getSizeAsSquare(int $width, int $height, int $actual_width, int $actual_height): array
    {
        if ($height < $width) {
            return $this->getSizeByFixedWidth($width, $actual_width, $actual_height);
        } else {
            if ($height > $width) {
                return $this->getSizeByFixedHeight($height, $actual_width, $actual_height);
            } else {
                // *** Square being resized to a square
                $optimal_width = $width;
                $optimal_height = $height;
            }
        }

        return [$optimal_width, $optimal_height];
    }

    protected function getSizeByAuto(int $width, int $height, int $actual_width, int $actual_height): array
    {
        if ($actual_height < $actual_width) {
            // *** Image to be resized is wider (landscape)
            [$optimal_width, $optimal_height] = $this->getSizeAsLandscape($width, $actual_width, $actual_height);
        } elseif ($actual_height > $actual_width) {
            // *** Image to be resized is taller (portrait)
            [$optimal_width, $optimal_height] = $this->getSizeAsPortrait($height, $actual_width, $actual_height);
        } else {
            // *** Image to be resized is a square
            [$optimal_width, $optimal_height] = $this->getSizeAsSquare($width, $height, $actual_width,
                $actual_height);
        }

        return [$optimal_width, $optimal_height];
    }

    protected function getSizeByFixedHeight(int $height, int $actual_width, int $actual_height): array
    {
        $ratio = $actual_width / $actual_height;
        $width = $height * $ratio;

        return [$width, $height];
    }

    protected function getSizeByFixedWidth(int $width, int $actual_width, int $actual_height): array
    {
        $ratio = $actual_height / $actual_width;
        $height = $width * $ratio;

        return [$width, $height];
    }

    protected function getWidthHeightFromImage($image): array
    {
        $image_width = imagesx($image);
        $image_height = imagesy($image);

        return [$image_width, $image_height];
    }

    protected function isStream($image_input): bool
    {
        return is_resource($image_input) && get_resource_type($image_input) === 'stream';
    }

    protected function resizeImageString(string $image_string)
    {
        $image = $this->createImageFromString($image_string);
        if ($image) {
            [$actual_width, $actual_height] = $this->getWidthHeightFromImage($image);
            $resized = $this->resizeImage($image, $this->getConfig('height'), $this->getConfig('width'), $actual_height,
                $actual_width, $this->getConfig('option'));
            imagedestroy($image);

            return $resized;
        }
    }

    protected function validateOption(string $option): bool
    {
        if (! isset($this->options[$option])) {
            throw new SizerException("Invalid option [$option].");
        }

        return true;
    }

    protected function validateOutputType(string $output): bool
    {
        if (! isset($this->outputs[$output])) {
            throw new SizerException("Invalid output type [$output].");
        }

        return true;
    }
}
