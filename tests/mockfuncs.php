<?php

namespace Smorken\Image\Sizer {

    use Mockery as m;

    function imagealphablending($rsrc, $blendmode)
    {
        return Mocks::imagealphablending($rsrc, $blendmode);
    }

    function imagesavealpha($rsrc, $saveflag)
    {
        return Mocks::imagesavealpha($rsrc, $saveflag);
    }

    function imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h)
    {
        return Mocks::imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
    }

    function imagecreatefromstring($image_str)
    {
        return Mocks::imagecreatefromstring($image_str);
    }

    function imagecreatetruecolor($width, $height)
    {
        return Mocks::imagecreatetruecolor($width, $height);
    }

    function imagedestroy($resource)
    {
        return Mocks::imagedestroy($resource);
    }

    function imagejpeg($image)
    {
        return Mocks::imagejpeg($image);
    }

    function imagepng($image)
    {
        return Mocks::imagepng($image);
    }

    function imagesx($image)
    {
        return Mocks::imagesx($image);
    }

    function imagesy($image)
    {
        return Mocks::imagesy($image);
    }

    function ob_start()
    {
        return Mocks::ob_start();
    }

    function ob_get_clean()
    {
        return Mocks::ob_get_clean();
    }

    class Mocks
    {
        /**
         * @var \Mockery\Mock
         */
        public static $functions;

        public static function __callStatic($name, $arguments)
        {
            if ($m = self::getMock()) {
                return call_user_func_array([$m, $name], $arguments);
            }
            $name = '\\'.$name;

            return call_user_func_array($name, $arguments);
        }

        public static function getMock()
        {
            if (isset($GLOBALS['mock']) && $GLOBALS['mock']) {
                if (! self::$functions) {
                    self::$functions = m::mock('functions');
                }

                return self::$functions;
            }
        }
    }
}
