<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/27/16
 * Time: 8:54 AM
 */

namespace Tests\Smorken\Image\Sizer\Unit;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Image\Sizer\Mocks;
use Smorken\Image\Sizer\Sizer;
use Smorken\Image\Sizer\SizerException;

include_once __DIR__.'/../mockfuncs.php';

class SizerTest extends TestCase
{
    public function setUp(): void
    {
        $GLOBALS['mock'] = true;
        Mocks::getMock()
            ->shouldReceive('ob_start');
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testAutoSizeSquareToLandscapeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('100', 100)
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '100',
                100,
                '400',
                '400'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['width' => 100, 'height' => 50]);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testAutoSizeSquareToPortraitWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('100', 100)
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '100',
                100,
                '400',
                '400'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['width' => 50, 'height' => 100]);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testAutoSizeToLandscapeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('200');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with(100, '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                100,
                '200',
                '200',
                '400'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo');
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testAutoSizeToPortraitWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('200');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with(400, '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                400,
                '200',
                '400',
                '200'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['option' => 'portrait']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testCropSizeHeightRatioGreaterThanWidthRatioWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('200');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with(400, 200)
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', '200')
            ->andReturn('new_bar_crop');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                400,
                200,
                '400',
                '200'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar_crop',
                'new_bar',
                0,
                0,
                100,
                0,
                '200',
                '200',
                '200',
                '200'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['option' => 'crop']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testCropSizeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('200');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with(200, 400)
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', '200')
            ->andReturn('new_bar_crop');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                200,
                400,
                '200',
                '400'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar_crop',
                'new_bar',
                0,
                0,
                0,
                100,
                '200',
                '200',
                '200',
                '200'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['option' => 'crop']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testExactSizeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('300');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '200',
                '200',
                '300',
                '400'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['option' => 'exact']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testGetConfigWithBadKeyReturnsNull()
    {
        $sut = $this->getSut();
        $this->assertNull($sut->getConfig('foo'));
    }

    public function testGetConfigWithNoKeyReturnsConfig()
    {
        $sut = $this->getSut();
        $this->assertEquals($this->getConfig(), $sut->getConfig());
    }

    public function testInvalidConfigurationOptionThrowsException()
    {
        $sut = $this->getSut();
        $this->expectException(SizerException::class);
        $this->expectExceptionMessage('out is not a valid configuration option.');
        $sut->size('foo', ['out' => 'foobar']);
    }

    public function testInvalidInputTypeThrowsException()
    {
        $sut = $this->getSut();
        $this->expectException(SizerException::class);
        $this->expectExceptionMessage(
            'Invalid image input.  Image input must be a stream or string.'
        );
        $sut->size(['array']);
    }

    public function testInvalidOptionThrowsException()
    {
        $sut = $this->getSut();
        $this->expectException(SizerException::class);
        $this->expectExceptionMessage('Invalid option [sizeme].');
        $sut->size('foo', ['option' => 'sizeme']);
    }

    public function testInvalidOutputThrowsException()
    {
        $sut = $this->getSut();
        $this->expectException(SizerException::class);
        $this->expectExceptionMessage('Invalid output type [foobar].');
        $sut->size('foo', ['output' => 'foobar']);
    }

    public function testLandscapeSizeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('300');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', 150)
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '200',
                150,
                '400',
                '300'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['option' => 'landscape']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testPortraitSizeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('300');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('400');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with(150, '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                150,
                '200',
                '300',
                '400'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['option' => 'portrait']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testSizeWithStream()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo image')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('1');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('1');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '200',
                '200',
                '1',
                '1'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $image_data = 'foo image';
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, $image_data);
        rewind($stream);
        $data = $sut->size($stream);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testSizeWithString()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('1');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('1');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '200',
                '200',
                '1',
                '1'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagejpeg')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/jpg',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo');
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testSizeWithStringCreatePng()
    {
        $sut = $this->getSut();
        Mocks::getMock()
            ->shouldReceive('imagecreatefromstring')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        Mocks::getMock()
            ->shouldReceive('imagesx')
            ->with('bar')
            ->once()
            ->andReturn('1');
        Mocks::getMock()
            ->shouldReceive('imagesy')
            ->with('bar')
            ->once()
            ->andReturn('1');
        Mocks::getMock()
            ->shouldReceive('imagecreatetruecolor')
            ->once()
            ->with('200', '200')
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagealphablending');
        Mocks::getMock()
            ->shouldReceive('imagesavealpha');
        Mocks::getMock()
            ->shouldReceive('imagecopyresampled')
            ->once()
            ->with(
                'new_bar',
                'bar',
                0,
                0,
                0,
                0,
                '200',
                '200',
                '1',
                '1'
            )
            ->andReturn('new_bar');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('imagepng')
            ->with('new_bar')
            ->once();
        Mocks::getMock()
            ->shouldReceive('ob_get_clean')
            ->once()
            ->andReturn('resized_image');
        Mocks::getMock()
            ->shouldReceive('imagedestroy')
            ->with('new_bar')
            ->once();
        $expected = [
            'mime' => 'image/png',
            'data' => 'resized_image',
            'size' => 13,
        ];
        $data = $sut->size('foo', ['output' => 'png']);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $data->$k);
        }
    }

    public function testSizerIsContractSizer()
    {
        $this->assertInstanceOf(\Smorken\Image\Sizer\Contracts\Sizer::class, $this->getSut());
    }

    protected function getConfig(): array
    {
        return [
            'height' => 200,
            'width' => 200,
            'option' => 'auto',
            'output' => 'jpg',
            'base64' => false,
        ];
    }

    protected function getSut(): \Smorken\Image\Sizer\Contracts\Sizer
    {
        return new Sizer($this->getConfig());
    }
}
