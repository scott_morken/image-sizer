<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/27/16
 * Time: 12:03 PM
 */

namespace Tests\Smorken\Image\Sizer\Integration;

use PHPUnit\Framework\TestCase;
use Smorken\Image\Sizer\Sizer;
use Smorken\Image\Sizer\SizerException;

class SizerTest extends TestCase
{
    public function setUp(): void
    {
        $GLOBALS['mock'] = false;
    }

    public function testCreateJpegFromInvalidFileStream()
    {
        $i = fopen(__DIR__.'/1x1.data', 'r+');
        $sut = $this->getSut();
        $data = $sut->size($i);
        $this->assertNull($data);
    }

    public function testCreateJpegFromInvalidString()
    {
        $sut = $this->getSut();
        $data = $sut->size('fiz buzz');
        $this->assertNull($data);
    }

    public function testCreateJpegFromNullIsException()
    {
        $sut = $this->getSut();
        $this->expectException(SizerException::class);
        $data = $sut->size(null);
    }

    public function testCreateJpegFromStream()
    {
        $i = fopen(__DIR__.'/1x1.png', 'r+');
        $sut = $this->getSut();
        $data = $sut->size($i);
        $enc = base64_encode($data['data']);
        $expected = '/9j/4AAQSkZJRgABAQEAYABgAAD//g';
        $this->assertStringStartsWith($expected, $enc);
        $this->assertEquals('image/jpg', $data['mime']);
    }

    public function testCreateJpegFromString()
    {
        $enc_string = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        $sut = $this->getSut();
        $data = $sut->size(base64_decode($enc_string));
        $enc = base64_encode($data['data']);
        $expected = '/9j/4AAQSkZJRgABAQEAYABgAAD//';
        $this->assertStringStartsWith($expected, $enc);
        $this->assertEquals('image/jpg', $data['mime']);
    }

    public function testCreatePngFromStream()
    {
        $i = fopen(__DIR__.'/1x1.png', 'r+');
        $sut = $this->getSut();
        $data = $sut->size($i, ['output' => 'png']);
        $enc = base64_encode($data['data']);
        $expected = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAA';
        $this->assertStringStartsWith($expected, $enc);
        $this->assertEquals('image/png', $data['mime']);
    }

    public function testCreatePngFromString()
    {
        $enc_string = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        $sut = $this->getSut();
        $data = $sut->size(base64_decode($enc_string), ['output' => 'png']);
        $enc = base64_encode($data['data']);
        $expected = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAA';
        $this->assertStringStartsWith($expected, $enc);
        $this->assertEquals('image/png', $data['mime']);
    }

    protected function getConfig(): array
    {
        return [
            'height' => 200,
            'width' => 200,
            'option' => 'auto',
            'output' => 'jpg',
            'base64' => false,
        ];
    }

    protected function getSut(): \Smorken\Image\Sizer\Contracts\Sizer
    {
        return new Sizer($this->getConfig());
    }
}
